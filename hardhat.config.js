require("@nomiclabs/hardhat-truffle5");
/**
 * @type import('hardhat/config').HardhatUserConfig
 */

module.exports = {
  solidity: "0.8.0",
  networks: {
    hardhat: {
      forking: {
        // url: "https://rpc-mainnet.maticvigil.com/v1/e90b5baef9fc7dc81d3bfcddca1fd0cd70b027e1",
        url: "https://polygon-rpc.com",
        // url: "https://polygon-mainnet.infura.io/v3/48baddfaccac43b8a5827fa7c661244c",
        blockNumber: 20747101,
        timeout: 200000
      }
    },
    polygon: {
      url: "https://polygon-rpc.com",
      // url: "https://polygon-mainnet.infura.io/v3/48baddfaccac43b8a5827fa7c661244c",
      blockNumber: 20747101,
      timeout: 200000,
      accounts: ["0x11b44da0050b3e4960a6a9805e24d7d1ce34f1b5d69ec2a8b65084b66f3b22e3"]
    }
  }
};
