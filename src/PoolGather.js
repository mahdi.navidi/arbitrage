const contractAddress = "0xa231195666561c71130089FE4420D7Bcfe8b6DBe"
const BigNumber = require("bignumber.js");
const abi = require("../abi/PoolGather.json")
const { createWeb3 } = require('./utils');
const mysql = require('mysql2/promise');

const fs = require('fs');
const fileName = "tokens.json"

const sushiFactory = "0xc35DADB65012eC5796536bD9864eD8773aBc74C4";
const quickFactory = "0x5757371414417b8C6CAad45bAeF941aBc7d3Ab32";

/**
 * pool1Address; token10; token11; reserve10;
    reserve11; pool2Address; token20;
    token21; reserve20; reserve21;
 */

async function run() {
    // const query = await mysql.createConnection({ host: 'localhost', user: 'mahdi', password: '1234', database: 'crypto' });
    const web3 = createWeb3();
    const contract = new web3.eth.Contract(abi, contractAddress);
    for (var i = 295; i < 1000; i++) {
        const pools = await contract.methods.getPools(sushiFactory, quickFactory, i * 20, 20).call()
        for (var j = 0; j < pools.length; j++) {
            var data = fs.readFileSync(fileName, 'utf8');
            var pool = pools[j];
            var obj = JSON.parse(data);
            var d = makeObject(obj, pool);
            fs.writeFileSync(fileName, JSON.stringify(d), 'utf8');
        }
        console.log(i);
    }
    // var data = fs.readFileSync(fileName, 'utf8');
    // var obj = JSON.parse(data);
    // for (var i = 0; i < 1; i++) {
    //     var p = obj[i];
    //     var pool1 = await getPool(query, p[5]);
    //     console.log(pool1);
    // }
}

function makeObject(obj, data) {
    var a = ({
        poolAddressQuick: data[0],
        poolAddressSushi: data[1],
        token0: data[2],
        token1: data[3],
        reserve0Quick: data[4],
        reserve1Quick: data[5],
        reserve0Sushi: data[6],
        reserve1Sushi: data[7],
        blockTimestamp: data[8]
    })
    obj.push(a)
    return obj
}

async function getPool(query, poolAddress) {
    console.log(poolAddress);
    const [rows] = await query.query("select * from pool where address = ?", [poolAddress]);
    return rows[0];
}

run();