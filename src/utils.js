const HDWalletProvider = require('@truffle/hdwallet-provider');
const Web3 = require('web3');

function waitForTxSuccess(tx) {
    return new Promise((accept, reject) => {
        try {
            tx.on('error', err => reject(err));
            tx.on('receipt', receipt => accept(receipt));
        } catch (err) {
            reject(err);
        }
    });
}

function createProvider() {
    const options = {
        keepAlive: true,
        timeout: 20000
    }
    // const url = "wss://rpc-mainnet.maticvigil.com/ws/v1/e90b5baef9fc7dc81d3bfcddca1fd0cd70b027e1";
    // const url = "https://rpc-mainnet.maticvigil.com/v1/e90b5baef9fc7dc81d3bfcddca1fd0cd70b027e1"
    // const url = "https://polygon-rpc.com";
    const url = "https://polygon-mainnet.g.alchemy.com/v2/3WNGsZqm_fqo3JlBeS0T_FvN_AkZX5mJ";
    // const url = "https://rinkeby.infura.io/v3/48baddfaccac43b8a5827fa7c661244c"
    const provider = new Web3.providers.HttpProvider(url);
    return provider;
    // const MNEMONIC = "child shield recipe keen proud recall oil square material file release pave";
    // return new HDWalletProvider({ mnemonic: MNEMONIC, providerOrUrl: provider, timeout: 10000 });
}

function createWeb3() {
    return new Web3(createProvider());
}

module.exports = {
    createWeb3,
    waitForTxSuccess,
    createProvider,
};

// address = 0x1F3E24E3cE0c05a90d570C584096cADE3afBC89C multiswap address