const { createWeb3 } = require('./utils');
const BigNumber = require('bignumber.js');
const mysql = require('mysql2/promise');
const fetch = require('node-fetch');
const fs = require('fs');

var headers = {
    'X-API-Key': 's6XZrh4AdFWnJkZ5u8used4Y74LO9mbqIG71EZTVRG5kWTYfUZMeVx7xuhdHEFcc'
}

const abi = require('../abi/PoolGather.json');
// const contractAddress = "0xCa82761656C240Cdd1DbD22d45a252f52aB6b1e3";
const contractAddress = "0x6dA2cC1CF1f7Ad071b87DBC688C4d6a5D35645ab";
// 0x29071f054e288eDD556cEfeFF0e29F4dF0833208

const fileName = "arbitrageTokens.json"
const sushiFactory = "0xc35DADB65012eC5796536bD9864eD8773aBc74C4";
const quickFactory = "0x5757371414417b8C6CAad45bAeF941aBc7d3Ab32";

async function run() {
    var data = fs.readFileSync(fileName, 'utf8');
    var obj = JSON.parse(data);
    const query = await mysql.createConnection({ host: 'localhost', user: 'mahdi', password: '1234', database: 'crypto' });
    const web3 = createWeb3();
    const contract = new web3.eth.Contract(abi, contractAddress);
    for (var j = 1731; j < 5000; j++) {
        // for (var j = 0; j < 1; j++) {
        const pools = await contract.methods.getPools(quickFactory, sushiFactory, j * 20, 20).call();
        for (var i = 0; i < pools.length; i++) {
            const pool = pools[i];
            // console.log(pool);
            // var decimal0 = await getDecimal(query, pool.token0);
            // var decimal1 = await getDecimal(query, pool.token1);
            // var r0q = new BigNumber(pool.reserve0Quick).shiftedBy(-1 * decimal0);
            // var r1q = new BigNumber(pool.reserve1Quick).shiftedBy(-1 * decimal1);
            // var r0s = new BigNumber(pool.reserve0Sushi).shiftedBy(-1 * decimal0);
            // var r1s = new BigNumber(pool.reserve1Sushi).shiftedBy(-1 * decimal1);
            // const p0 = new BigNumber(r1q).dividedBy(r0q);
            // const p1 = new BigNumber(r1s).dividedBy(r0s);
            // console.log(pool.token0, p0.minus(p1).toFixed(), pool.token1);
            // var data = fs.readFileSync(fileName, 'utf8');
            var obj = JSON.parse(data);
            // var json = JSON.stringify(pool);
            var d = makeObject(obj, pool);
            fs.writeFileSync(fileName, JSON.stringify(d), 'utf8');
        }
        console.log(j);
    }
    console.log('finished');
}

function makeObject(obj, data) {
    var a = ({
        poolAddressQuick: data[0],
        poolAddressSushi: data[1],
        token0: data[2],
        token1: data[3],
        reserve0Quick: data[4],
        reserve1Quick: data[5],
        reserve0Sushi: data[6],
        reserve1Sushi: data[7],
        blockTimestamp: data[8]
    })
    obj.push(a)
    return obj
}

async function getDecimal(query, token) {
    const [rows] = await query.query("select decimals from token where address = ?", [token]);
    return rows[0].decimals
}

async function getToken(query, tokenAddress) {
    const [rows] = await query.query("select * from token where address = ?", [tokenAddress]);
    if (rows[0] != undefined) {

    }
}

run();