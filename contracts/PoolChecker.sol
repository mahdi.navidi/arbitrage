pragma solidity ^0.8.0;
pragma experimental ABIEncoderV2;

import "./IUniswapV2Router.sol";

// contract address 0xCa82761656C240Cdd1DbD22d45a252f52aB6b1e3
contract PoolChecker {
    IUniswapV2Factory factoryQuick;
    IUniswapV2Factory factorySushi;

    struct Pool {
        address poolAddressQuick;
        address poolAddressSushi;
        address token0;
        address token1;
        uint256 reserve0Quick;
        uint256 reserve1Quick;
        uint256 reserve0Sushi;
        uint256 reserve1Sushi;
        uint256 blockTimestamp;
    }

    Pool[] pools;

    // factoryAddress = 0x5757371414417b8C6CAad45bAeF941aBc7d3Ab32

    constructor(address factoryAddressQuick, address factoryAddressSushi)
        public
    {
        factoryQuick = IUniswapV2Factory(factoryAddressQuick);
        factorySushi = IUniswapV2Factory(factoryAddressSushi);
    }

    function getLengthQuick() public view returns (uint256) {
        return factoryQuick.allPairsLength();
    }

    function getLengthSushi() public view returns (uint256) {
        return factorySushi.allPairsLength();
    }

    function getPools(uint256 from, uint256 count)
        public
        view
        returns (Pool[] memory)
    {
        Pool[] memory p = new Pool[](count);
        uint256 j = from;
        for (uint256 i = 0; i < count; i++) {
            IUniswapV2Pair pair = IUniswapV2Pair(factorySushi.allPairs(j));
            (
                uint256 reserve0Sushi,
                uint256 reserve1Sushi,
                uint256 blockTimestamp
            ) = pair.getReserves();
            (
                address quickAddresss,
                uint256 reserve0Quick,
                uint256 reserve1Quick
            ) = getQuickPool(pair.token0(), pair.token1());
            Pool memory pool = Pool({
                poolAddressQuick: quickAddresss,
                poolAddressSushi: factorySushi.allPairs(j),
                token0: pair.token0(),
                token1: pair.token1(),
                reserve0Quick: reserve0Quick,
                reserve1Quick: reserve1Quick,
                reserve0Sushi: reserve0Sushi,
                reserve1Sushi: reserve1Sushi,
                blockTimestamp: blockTimestamp
            });
            p[i] = pool;
            j++;
        }
        return p;
    }

    function getQuickPool(address token0, address token1)
        internal
        view
        returns (
            address,
            uint256,
            uint256
        )
    {
        address quickPairAddress = factoryQuick.getPair(token0, token1);
        if (quickPairAddress == address(0)) {
            quickPairAddress = factoryQuick.getPair(token1, token0);
            if (quickPairAddress == address(0)) {
                return (address(0), 0, 0);
            }
            IUniswapV2Pair pair = IUniswapV2Pair(quickPairAddress);
            (uint256 reserve0, uint256 reserve1, uint256 blockTimestamp) = pair
                .getReserves();
            return (quickPairAddress, reserve1, reserve0);
        }
        IUniswapV2Pair pair = IUniswapV2Pair(quickPairAddress);
        (uint256 reserve0, uint256 reserve1, uint256 blockTimestamp) = pair
            .getReserves();
        return (quickPairAddress, reserve0, reserve1);
    }
}
