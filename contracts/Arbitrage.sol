pragma solidity ^0.8.0;

// import "hardhat/console.sol";
import "./IUniswapV2Router.sol";

// import "@uniswap/v2-core/contracts/UniswapV2Pair.sol";

contract Arbitrage {
    address QUICK_SWAP_ROUTER = 0xa5E0829CaCEd8fFDD4De3c43696c57F7D7A678ff;
    address SUSHI_SWAP_ROUTER = 0x1b02dA8Cb0d097eB8D57A175b88c7D8b47997506;
    address QUICK_SWAP_FACTORY = 0x5757371414417b8C6CAad45bAeF941aBc7d3Ab32;
    address SUSHI_SWAP_FACTORY = 0xc35DADB65012eC5796536bD9864eD8773aBc74C4;
    IUniswapV2Router router;
    IUniswapV2Pair pair;
    IUniswapV2Factory factory;

    constructor() public {}

    address[] public addresses;

    function getToken(address token) public view returns (uint256) {
        for (uint256 i = 0; i < addresses.length; i++) {
            if (addresses[i] == token) {
                return i;
            }
        }
    }

    function addressLength() public view returns(uint){
        return addresses.length;
    }

    function createPair(address token) external {
        require(token != address(0), "ZERO_ADDRESS");
        require(getToken(token) == 0, "TOKEN_EXISTS");
        addresses.push(token);
    }

    function findOpportunity()
        public
        view
        returns (
            address,
            address,
            address,
            address
        )
    {
        for (uint256 i = 0; i < addresses.length; i++) {
            address token0 = addresses[i];
            for (uint256 j = 0; j < addresses.length; j++) {
                address token1 = addresses[j];
                (
                    address t0,
                    address t1,
                    address f0,
                    address f1
                ) = checkArbitrage(token0, token1);
                if (t0 == address(0)) {
                    continue;
                } else {
                    return (t0, t1, f0, f1);
                }
            }
        }
        return (address(0), address(0), address(0), address(0));
    }

    function checkArbitrage(address token0, address token1)
        internal
        view
        returns (
            address,
            address,
            address,
            address
        )
    {
        require(token0 != address(0), "token0 is 0");
        require(token1 != address(0), "token1 is 0");
        // console.log("inputs, %s, %s, %s, %s", token0, token1, QUICK_SWAP_FACTORY, SUSHI_SWAP_FACTORY);
        uint256 swap1 = getPrice(
            token0,
            token1,
            QUICK_SWAP_FACTORY,
            SUSHI_SWAP_FACTORY
        );
        uint256 swap2 = getPrice(
            token1,
            token0,
            SUSHI_SWAP_FACTORY,
            QUICK_SWAP_FACTORY
        );
        if (swap2 > swap1) {
            // return arbitrage is profitable
            return (token0, token1, QUICK_SWAP_FACTORY, SUSHI_SWAP_FACTORY);
        } else {
            swap1 = getPrice(
                token1,
                token0,
                QUICK_SWAP_FACTORY,
                SUSHI_SWAP_FACTORY
            );
            swap2 = getPrice(
                token0,
                token1,
                SUSHI_SWAP_FACTORY,
                QUICK_SWAP_FACTORY
            );
            if (swap2 > swap1) {
                // return arbitrage is profitable
                return (token1, token0, QUICK_SWAP_FACTORY, SUSHI_SWAP_FACTORY);
            }
        }
        return (address(0), address(0), address(0), address(0));
    }

    function getPairPrice(
        address token0,
        address token1,
        IUniswapV2Pair pair
    ) internal view returns (uint256) {
        (uint256 reserve0, uint256 reserve1, uint256 blockTimestampLast) = pair
            .getReserves();
        if (pair.token0() == token0 && pair.token1() == token1) {
            return reserve1 / reserve0;
        }
        if (pair.token1() == token0 && pair.token0() == token1) {
            return reserve0 / reserve1;
        }
    }

    function getPrice(
        address token0,
        address token1,
        address r1,
        address r2
    ) internal view returns (uint256) {
        IUniswapV2Factory factory1 = IUniswapV2Factory(r1);
        address pairAddress1 = factory1.getPair(token0, token1);
        IUniswapV2Pair pair1;
        IUniswapV2Pair pair2;
        if (pairAddress1 == address(0)) {
            pairAddress1 = factory1.getPair(token1, token0);
            if (pairAddress1 == address(0)) {
                return 0;
            }
        }
        pair1 = IUniswapV2Pair(pairAddress1);
        IUniswapV2Factory factory2 = IUniswapV2Factory(r2);
        address pairAddress2 = factory2.getPair(token0, token1);
        if (pairAddress2 == address(0)) {
            pairAddress2 = factory2.getPair(token1, token0);
            if (pairAddress2 == address(0)) {
                return 0;
            }
        }
        pair2 = IUniswapV2Pair(pairAddress2);
        uint256 price1 = getPairPrice(token0, token1, pair1);
        uint256 price2 = getPairPrice(token0, token1, pair2);
        if (price1 > price2) {
            return price1;
        }
        if (price2 > price1) {
            return price2;
        }
        return price1;
    }
}
