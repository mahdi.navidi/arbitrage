pragma solidity ^0.8.0;

import "./UniswapFlashSwapper.sol";

contract ArbitrageSwap is UniswapFlashSwapper {

    address owner;
    bytes _params;

    constructor(address factoryAddress) public UniswapFlashSwapper(factoryAddress)
    {
        owner = msg.sender;
    }

    function flashSwap(
        uint256 amount,
        address token0,
        address token1,
        address pairToken,
        address router0Address,
        address router1Address
        ) external
    {
            _params =  abi.encode(token0, token1, amount, router0Address, router1Address);
            startSwap(token0, amount, pairToken, _params);
    }

    function execute(address _tokenBorrow, uint _amount, address _tokenPay, uint _amountToRepay, bytes memory _userData) override internal
    {
        address token0;
        address token1;
        uint amount;
        address router0Address;
        address router1Address;
        (token0, token1, amount, router0Address, router1Address) = abi.decode(_params, (address, address, uint, address, address));
        require(token0 != address(0), "t0 is empty");
        require(token1 != address(0), "t1 is empty");
        doSwaps(amount, token0, token1,router0Address, router1Address);
    }

    function doSwaps(
        uint256 amount,
        address token0Address,
        address token1Address,
        address router0Address,
        address router1Address
    ) internal {
        swap(amount, token0Address, token1Address, router0Address);
        IERC20 token1 = IERC20(token1Address);
        swap(
            token1.balanceOf(address(this)),
            token1Address,
            token0Address,
            router1Address
        );
    }

    function swap(
        uint256 amount,
        address token0,
        address token1,
        address routerAddress
    ) internal {
        IUniswapV2Router router = IUniswapV2Router(routerAddress);
        require(IERC20(token0).approve(routerAddress, amount), 'approve failed.');
        require(IERC20(token1).approve(routerAddress, amount), 'approve2 failed.');
        address[] memory path = new address[](2);
        path[0] = token0;
        path[1] = token1;
        router.swapExactTokensForTokens(
            amount,
            0,
            path,
            address(this),
            block.timestamp
        );
    }

    function withdrawToken(IERC20 token)
        public
    {
        require(token.transfer(owner, token.balanceOf(address(this))));
    }

    function balanceOfToken(IERC20 token) public view returns(uint){
        return token.balanceOf(address(this));
    }
}
