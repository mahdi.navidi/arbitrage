pragma solidity ^0.8.0;

import "./IUniswapV2Router.sol";

contract PoolGather {
    struct ArbitragePool {
        address pool1Address;
        address token10;
        address token11;
        uint256 reserve10;
        uint256 reserve11;
        address pool2Address;
        address token20;
        address token21;
        uint256 reserve20;
        uint256 reserve21;
    }

    struct Pool {
        address poolAddress;
        address token0;
        address token1;
        uint256 reserve0;
        uint256 reserve1;
    }

    function getLength(address factoryAddress) public view returns (uint256) {
        return IUniswapV2Factory(factoryAddress).allPairsLength();
    }

    function getPools(
        address factory1Address,
        address factory2Address,
        uint256 from,
        uint256 count
    ) public view returns (ArbitragePool[] memory) {
        IUniswapV2Factory factory1 = IUniswapV2Factory(factory1Address);

        ArbitragePool[] memory p = new ArbitragePool[](count);
        uint256 i = 0;
        uint256 j = from;
        for(uint k = 0; k < count; k++){
            address pool1Address = factory1.allPairs(j);
            j++;
            IUniswapV2Pair pair1 = IUniswapV2Pair(pool1Address);
            (uint256 reserve10, uint256 reserve11,) = pair1
                .getReserves();
            address pool2Address = getPair2(
                factory2Address,
                pair1.token0(),
                pair1.token1()
            );
            if (pool2Address == address(0)) {
                continue;
            }
            IUniswapV2Pair pair2 = IUniswapV2Pair(pool2Address);
            (uint256 reserve20, uint256 reserve21,) = pair2
                .getReserves();
            ArbitragePool memory pool = ArbitragePool({
                pool1Address: pool1Address,
                token10: pair1.token0(),
                token11: pair1.token1(),
                reserve10: reserve10,
                reserve11: reserve11,
                pool2Address: pool2Address,
                token20: pair2.token0(),
                token21: pair2.token1(),
                reserve20: reserve20,
                reserve21: reserve21
            });
            p[i] = pool;
            i++;
        }
        return p;
    }

    function getPair2(
        address factory2Address,
        address token0,
        address token1
    ) internal view returns (address) {
        IUniswapV2Factory factory = IUniswapV2Factory(factory2Address);
        address pool = factory.getPair(token0, token1);
        if (pool == address(0)) {
            pool = factory.getPair(token1, token0);
        }
        return pool;
    }

    function getPoolsByAddress(address[] memory poolAddresses)
        public
        view
        returns (Pool[] memory)
    {
        uint256 count = poolAddresses.length;
        Pool[] memory p = new Pool[](count);
        for (uint256 i = 0; i < count; i++) {
            address poolAddress = poolAddresses[i];
            IUniswapV2Pair pair = IUniswapV2Pair(poolAddress);
            (uint256 reserve0, uint256 reserve1, ) = pair.getReserves();
            Pool memory pool = Pool({
                poolAddress: poolAddress,
                token0: pair.token0(),
                token1: pair.token1(),
                reserve0: reserve0,
                reserve1: reserve1
            });
            p[i] = pool;
        }
        return p;
    }
}
