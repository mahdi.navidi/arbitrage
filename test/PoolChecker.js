const PoolChecker = artifacts.require("PoolChecker");
const BigNumber = require('bignumber.js');

contract('PoolChecker', (accounts) => {
    it('begin', async function () {
        const pool = await PoolChecker.new("0x5757371414417b8C6CAad45bAeF941aBc7d3Ab32", "0xc35DADB65012eC5796536bD9864eD8773aBc74C4");

        // console.log(new BigNumber(await pool.getLengthQuick()).toFixed());
        // console.log(new BigNumber(await pool.getLengthSushi()).toFixed());
        console.log(await pool.getPools(0, 5));
    })
})
